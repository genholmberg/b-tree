#ifndef leaf_list_records_
#define leaf_list_records_
#include "globallib.hpp"
#include "global.h"
#include <list>
#include <vector>
using namespace std;

template<class item_type>
class leaf_node
{
	private:
		size_t type;
		size_t leaf_size;
		size_t pre_leaf;
		size_t next_leaf;
		list<item_type> list_records;

	public:
		leaf_node()
		{
			type = 2;
			leaf_size = 0;
			pre_leaf = 0;
			next_leaf = 0;
		}

		size_t get_pre_leaf()
		{
			return pre_leaf;
		}

		size_t get_next_leaf()
		{
			return next_leaf;
		}

		size_t get_type()
		{
			return type;
		}
		size_t get_size()
		{
			return leaf_size;
		}

		list<item_type> get_records()
		{
			return list_records; 
		}

		void from_string(string leaf_str)
		{
			vector<string> parsed_str = ParseString(leaf_str);

			leaf_size = Decode(parsed_str[0]);
			type = Decode(parsed_str[1]);
			pre_leaf = Decode(parsed_str[2]);
			next_leaf = Decode(parsed_str[3]);

			for(int i = 4; i < parsed_str.size(); i++)
			{
				item_type temp;
				temp = parsed_str[i];
				list_records.push_back(temp);
			}
		}

		string to_string()
		{
			string to_string;
			string to_string_size;
			string temp_str;
			temp_str = Encode(type, SHORT_CHAR);
			to_string = temp_str + delimiter;

			temp_str = Encode(pre_leaf, LONG_CHAR);
			to_string = to_string + temp_str + delimiter;

			temp_str = Encode(next_leaf, LONG_CHAR);
			to_string = to_string + temp_str + delimiter;

			typename std::list<item_type>::iterator iter = list_records.begin();
			while (iter != list_records.end())
			{
				to_string = to_string + *iter + delimiter;
				iter++;
			}

			// calculate size
			leaf_size = to_string.length() + 1;
			to_string_size = Encode(leaf_size, SHORT_CHAR);
			to_string = to_string_size + to_string;
			return to_string;
		}

		void print_leaf()
		{
			cout << "Type: " << type << "\nLeaf size " << leaf_size << "\npreivous leaf: " << pre_leaf << "\nnext leaf: " << next_leaf << endl;
			typename std::list<item_type>::iterator iter = list_records.begin();
			while (iter != list_records.end())
			{
				cout << "Record: " << *iter << endl;
				iter++;
			}
		}
};

#endif