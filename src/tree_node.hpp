#ifndef tree_node_
#define tree_node_
#include "globallib.hpp"
#include "global.h"
#include <iostream>
#include <fstream>
#include <list>
#include <vector>
using namespace std;

template<typename item_type>
struct key_offset
{
	item_type key;
	size_t child_offset;
};

static const char delimiter = static_cast<char>(DELIMITER);
template<class item_type>
class tree_node
{
	private:
		size_t type;
		size_t node_size;
		size_t parent_offset;

		list< key_offset<item_type> > node;

	public:
		tree_node()
		{
			type = 1;
			node_size = 0;
			parent_offset = 0;

		}

		size_t get_offest_child(item_type a_key)
		{
			typename std::list< key_offset<item_type> >::iterator iter = node.begin();
			while (iter != node.end())
			{
					if(iter->key == a_key)
						return iter->child_offset;
					else
						iter++;
			}
		}

		size_t get_parent_offset()
		{
			return parent_offset;
		}

		size_t get_type()
		{
			return type;
		}

		size_t get_size()
		{
			return node_size;
		}

		void from_string(string node_str)
		{
			vector<string> parsed_str = ParseString(node_str);
			struct key_offset<item_type> temp;

			node_size = Decode(parsed_str[0]);
			type = Decode(parsed_str[1]);
			parent_offset = Decode(parsed_str[2]);

			for(int i = 3; i < parsed_str.size() - 1; i++)
			{
				temp.key = parsed_str[i];
				i++;
				temp.child_offset = Decode(parsed_str[i]);
				node.push_back(temp);
			}
		}

		string to_string()
		{
			string to_string;
			string to_string_size;
			string temp_str;
			temp_str = Encode(type, SHORT_CHAR);
			to_string = temp_str + delimiter;

			temp_str = Encode(parent_offset, LONG_CHAR);
			to_string = to_string + temp_str + delimiter;

			typename std::list< key_offset<item_type> >::iterator iter = node.begin();
			while (iter != node.end())
			{
				string child_offset = Encode(iter->child_offset, LONG_CHAR);
				to_string = to_string + iter->key + delimiter + child_offset + delimiter;
				iter++;
			}

			// calculate size
			node_size = to_string.length() + 1;
			to_string_size = Encode(node_size, SHORT_CHAR);
			to_string = to_string_size + to_string;
			return to_string;
		}

		void print_node()
		{
			cout << "Type: " << type << "\nNode size " << node_size << "\nParent offset: " << parent_offset << endl;
			typename std::list< key_offset<item_type> >::iterator iter = node.begin();
			while (iter != node.end())
			{
				cout << "Key: " << iter->key << "\tChild offset: " << iter->child_offset << endl;
				iter++;
			}
		}
};

#endif