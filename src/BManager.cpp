#include "BManager.h"

// CONSTRUCTORS / DESTRUCTORS

BManager::BManager(const std::string& file_name)
{
    // Try open the file.
    file_ = fopen(file_name.c_str(), "r+");
    if (file_ == nullptr)
    {
        throw std::invalid_argument("Cannot open file: " + file_name);
    }

    // Create a header if there is none.
    if (fseek(file_, 0, SEEK_END) == 0)
    {
        rewind(file_);

        // Write header string.
    }
    else
    {
        // Read header string.
    }
}

BManager::~BManager()
{
    if(fclose(file_) == EOF)
    {
        throw std::ios_base::failure("Cannot close file");
    }
}

// END CONSTRUCTORS / DESTRUCTORS

// METHODS

std::string BManager::Read(const size_t& offset)
{
    std::string record;
    size_t bytes_read = 0;
    size_t bytes_to_read = GetSize(offset);
    char c;

    // Move to the offset.
    fseek(file_, SEEK_SET, offset);

    for (size_t i = 0; i < bytes_to_read; ++i)
    {
        bytes_read = fread(&c, sizeof(char), 1, file_);
        if (bytes_read == 0)
        {
            // Error occurred.
            break;
        }

        record.push_back(c);
    }

    return record;
}

// END METHODS

// HELPER FUNCTIONS

size_t BManager::GetSize(const size_t& offset)
{
    char temp;
    std::string buffer;
    size_t bytes_read = 0;

    // Move to the offset.
    fseek(file_, SEEK_SET, offset);

    // Collect the size of the record.
    bytes_read = fread(&temp, sizeof(char), 1, file_);
    while ((temp != DELIMITER) && (bytes_read > 0))
    {
        buffer.push_back(temp);
        bytes_read = fread(&temp, sizeof(char), 1, file_);
    }

    // Decode and return the length.
    return Decode(buffer);
}

// END HELPER FUNCTIONS
