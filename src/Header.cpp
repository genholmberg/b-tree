#include "Header.h"

// CONSTRUCTOR / DESTRUCTOR

Header::Header()
{
    size_ = HEADER_SIZE;
    type_ = HEAD;
    root_offset_ = NONE;
    list_offset_ = NONE;
    free_offset_ = HEADER_SIZE + 1;
}

// SEND CONSTRUCTOR / DESTRUCTOR

// GETTERS / SETTERS

size_t Header::GetSize() const
{
    return size_;
}

size_t Header::GetRootOffset() const
{
    return root_offset_;
}

size_t Header::GetListOffset() const
{
    return list_offset_;
}

size_t Header::GetFreeOffset() const
{
    return free_offset_;
}

void Header::SetSize(const size_t& size)
{
    size_ = size;
}

void Header::SetRootOffset(const size_t& root_offset)
{
    root_offset_ = root_offset;
}

void Header::SetListOffset(const size_t& list_offset)
{
    list_offset_ = list_offset;
}

void Header::SetFreeOffset(const size_t& free_offset)
{
    free_offset_ = free_offset;
}


// END GETTERS / SETTERS

// METHODS

std::string Header::ToString() const
{
    std::string header_string;
    std::stringstream stream;

    // Build the string.
    stream << Encode(size_, SHORT_CHAR)       << DELIMITER
           << Encode(type_, SHORT_CHAR)       << DELIMITER
           << Encode(root_offset_, LONG_CHAR) << DELIMITER
           << Encode(list_offset_, LONG_CHAR) << DELIMITER
           << Encode(free_offset_, LONG_CHAR) << DELIMITER;
    header_string = stream.str();
    stream.str("");
    stream.clear();

    // Pack the remainder of the header.
    for (size_t i = header_string.size(); i < HEADER_SIZE; ++i)
    {
        header_string.push_back(DELIMITER);
    }

    return header_string;
}

void Header::FromString(const std::string& header)
{
    // Collect the values.
    std::vector<std::string> values = ParseString(header);
    std::vector<std::string>::iterator index = values.begin();

    // Copy values into fields.
    size_ = Decode(*index);
    ++index;

    // Increment past the type (we know this is a header).
    ++index;

    root_offset_ = Decode(*index);
    ++index;

    list_offset_ = Decode(*index);
    ++index;

    free_offset_ = Decode(*index);;
    ++index;
}

std::string Header::Print() const
{
    std::stringstream stream;

    stream << size_         << "\t"
           << type_         << "\t"
           << root_offset_  << "\t"
           << list_offset_  << "\t"
           << free_offset_;
    
    return stream.str();
}

// END METHODS
