#ifndef _GLOBAL_LIB_H
#define _GLOBAL_LIB_H

#include <cstddef>
#include <string>
#include <vector>

#include <iostream> // Delete

#include "global.h"

// Encode size into characters.
inline std::string Encode(const size_t& size, const EncodeType& length)
{
    int mask = 0xff;
    std::string code;
    
    switch (length)
    {
        case SHORT_CHAR:
            code.push_back(static_cast<char>(mask & size));
            break;

        case LONG_CHAR:
            code.push_back(static_cast<char>(mask & (size >> 8)));
            code.push_back(static_cast<char>(mask & size));
            break;

        default:
            // This should never occur.
            break;
    }

    return code;
}

// Decode a set of characters into a size.
inline size_t Decode(const std::string& code)
{
    // 'position' is an int because it may go below zero.
    int position = static_cast<int>(code.size() - 1);
    size_t temp = 0;
    size_t value = 0;

    for (size_t i = 0; i < code.size(); ++i)
    {
        // Collect the next byte. (i.e. the next set of most significant bits.)
        temp = static_cast<size_t>(code[i]);

        // Right shift temp 'position' bits. This puts the set of bits into their
        // corresponding locations in an actual number.
        temp = temp << (position * 8);

        // Save the bits into the number.
        value = temp | value;

        --position;
    }

    return value;
}

// Parse a string into a vector of size_t items.
inline std::vector<std::string> ParseString(const std::string& str)
{
    // Start and end offsets of each substring.
    size_t start = 0;
    size_t end = 0;
    std::vector<std::string> items;
    std::string substring;

    // Loop through the entire string collecting each substring value.
    end = str.find_first_of(DELIMITER, start);
    while (end != std::string::npos)
    {
        // Save the sub string, provided it has some data (i.e. !! would have
        // no data, while !4! would have 4 as its data).
        substring = str.substr(start, end - start);
        if (substring.size() != 0)
        {
            items.push_back(substring);
        }

        start = end + 1;
        end = str.find_first_of(DELIMITER, start);
    }

    return items;
}

#endif
