#ifndef _FILE_H
#define _FILE_H

#include <cstddef>

// Types:

// These are set to ASCII values, so we can encode and decode them just like
// we do with all the other character encodings, but this way we can "read"
// the node's type.
typedef enum
{
    HEAD = 48, // 0
    NODE = 49, // 1
    LIST = 50  // 2
} NodeType;

typedef enum
{
    SHORT_CHAR  = 1, // Encode only one character.
    LONG_CHAR   = 2  // Encode two characters.
} EncodeType;

// Meta-characters:
static const char   DELIMITER   = 0x21; // '!' or 033.
static const size_t HEADER_SIZE = 50;

// Sizes:
static const size_t NONE        = 0;

#endif

