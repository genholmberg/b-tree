#ifndef _FREE_H
#define _FREE_H

#include <cstddef>
#include <deque>
#include <vector>

#include "global.h"
#include "globallib.h"

typedef struct
{
    size_t b_size = SIZE_MAX;
    size_t b_offset = 0;
    size_t b_next = NONE;
    size_t b_previous = NONE;
} Block;

typedef std::deque<Block>::iterator iter;

// Minimum number of "extra" bytes needed when considering free space.
// i.e. a free spot must be at least MIN_BYTES larger than the
// actually minimum. This allows us to keep track of all free spaces.
static const size_t MIN_BYTES = 5;

class Free
{
private:
    std::deque<Block> free_list_;
    
    iter index_;

    // Reset the iterator to the beginning of the list.
    inline void Reset(std::deque<Block>::iterator i) { i = free_list_.begin(); }


public:
    Free();
    explicit Free(const std::string);

    // Find an offset that can fit the given size.
    size_t FindFreeBlock(const size_t&);

    // Fill a free block at this offset with this many bytes.
    void UseFreeBlock(const size_t&, const size_t&);

    // Create a string representation of a free block to write to disk.
    std::string ToString() const;

    // Parse a string representation of a free block and add to list.
    void FromString(const std::string&);
};

#endif
