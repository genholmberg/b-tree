#ifndef _B_MANAGER_H
#define _B_MANAGER_H

#include <cstdio>
#include <cstdlib>
#include <exception>
#include <list>
#include <stdexcept>
#include <string>
#include <sstream>

#include "global.h"
#include "globallib.hpp"

class BManager
{
private:
    FILE* file_;

    // Reads the size and type of the record at the given offset.
    // Returns the size and type of that record.
    size_t GetSize(const size_t&);

public:
    explicit BManager(const std::string&);
    ~BManager();

    // Read a record from disk at the given offset.
    // Return the record as a string.
    std::string Read(const size_t&);

    // Find an appropriate spot to write a record string to in file.
    // Returns an offset to the selected location.
    //
    //size_t FindFree(const std::string&);

    // Given an offset and a string, write that string at that offset.
    // Returns true for success, false for failure.
    //
    //bool Write(const std::string&);
};

#endif
